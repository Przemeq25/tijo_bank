package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountsStorage implements AccountService {

    Map<Integer, Account> accounts = new HashMap<Integer, Account>();

    @Override
    public Account getAccountByNumber(int accountNumber) {
        return accounts.get(accountNumber);
    }

    @Override
    public Account saveAccount(Account account) {
        return accounts.put(account.getAccountNumber(),account);
    }

    @Override
    public void deleteAccount(int accountNumber) {
        accounts.remove(accountNumber);
    }

    @Override
    public List<Account> findAllAccounts() {
        return new ArrayList<>(accounts.values());
    }
}
